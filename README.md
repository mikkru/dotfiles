# dotfiles
To use this on a new account/computer:

1. Configure ssh access to gitlab (`ssh-add -K private.key` on osx)

1. Run commands:
```bash
# Clone BARE repo
git clone --bare git@gitlab.com:mikkru/dotfiles.git ~/.dotfiles/.git

# Setup alias and configure git
alias dotfiles='/usr/bin/git --git-dir="${HOME}/.dotfiles/.git/" --work-tree="${HOME}"'
dotfiles config --local user.name "mikkru"
dotfiles config --local user.email "none@nowhere.com" 
dotfiles config --local status.showUntrackedFiles no

# Fast forward to get all files in place
dotfiles checkout
```

